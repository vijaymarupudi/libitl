#!/usr/bin/env python3

top = '.'
out = 'build'

includes = ["deps/freetype/include", "deps/harfbuzz/src", "deps/json/single_include", "deps/tree-sitter/lib/include", 'src/']

import os
from waflib import Options

def options(ctx):
    pass

def configure(ctx):
    ctx.add_os_flags('CFLAGS')
    ctx.add_os_flags('CXXFLAGS')
    ctx.add_os_flags('CC')
    ctx.add_os_flags('CXX')
    ctx.add_os_flags('LD')
    ctx.add_os_flags('AR')
    
    ctx.env.CC = ctx.env['CC'] or 'gcc'
    ctx.env.CXX = ctx.env['CXX'] or 'g++'
    ctx.env.LD = ctx.env['LD'] or 'g++'
    ctx.env.AR = ctx.env['AR'] or 'ar'

    if not ctx.env.CFLAGS:
        ctx.env.CFLAGS = ['-O3']
    if not ctx.env.CXXFLAGS:
        ctx.env.CXXFLAGS = ['-O3']
    
    full_paths = ['-I ' + ctx.path.abspath() + "/" + path for path in includes]
    ctx.env.append_value('INCLUDES', full_paths)
    ctx.env.append_value('CFLAGS', ['-fPIC'])
    ctx.env.append_value('CXXFLAGS', ['-fPIC'])

def build(ctx):

    include_flags = " ".join("-I " + ctx.path.abspath() + "/" + path for path in includes)
    
    # build harfbuzz
    ctx(rule='${CXX} ${CPPFLAGS} ${CXXFLAGS} -DHAVE_FREETYPE -I ' + ctx.path.abspath() + "/deps/freetype/include" + ' -c ${SRC} -o ${TGT}', source='deps/harfbuzz/src/harfbuzz.cc', target='deps/harfbuzz/src/harfbuzz.o')

    # build freetype
    freetype_out = ctx.bldnode.make_node('deps/freetype/libfreetype.a')
    freetype_dir = ctx.srcnode.abspath() + "/deps/freetype"
    ctx(rule=f"cd {freetype_dir} && make setup ansi && make CFLAGS='{' '.join(ctx.env.CFLAGS)} -c -fPIC' -j8 && cp objs/libfreetype.a {freetype_out.abspath()}", target=freetype_out, shell=True)

    # build tree-sitter
    treesitter_out = ctx.bldnode.make_node('deps/tree-sitter/libtree-sitter.a')
    treesitter_dir = ctx.srcnode.abspath() + "/deps/tree-sitter"
    ctx(rule=f"cd {treesitter_dir} && make -j8 && cp libtree-sitter.a {treesitter_out.abspath()}", target=treesitter_out, shell=True)

    # build itl
    source_files = ['src/itl-code.cpp', 'src/highlights/python.c', 'src/itl.cpp', 'src/noto-serif.c', 'src/stb_image_write_implementation.c', 'deps/tree-sitter-python/src/scanner.cc', 'deps/tree-sitter-python/src/parser.c']

    targets = []

    for f in source_files:
        rule = None
        if os.path.splitext(f)[1] == '.cpp' or os.path.splitext(f)[1] == '.cc':
            rule = '${CXX} ${CXXFLAGS} ${CPPFLAGS} ${INCLUDES} -c ${SRC} -o ${TGT}'
        else:
            rule = '${CC} ${CFLAGS} ${CPPFLAGS} ${INCLUDES} -c ${SRC} -o ${TGT}'

        target = os.path.splitext(f)[0] + ".o"
        ctx(rule=rule, source=f, target=target)
        targets.append(target)


    libitl_objs = targets + ['deps/freetype/libfreetype.a', 'deps/harfbuzz/src/harfbuzz.o', 'deps/tree-sitter/libtree-sitter.a']

    ctx(rule="${CXX} ${CXXFLAGS} ${CPPFLAGS} ${INCLUDES} ${SRC} -o generate-image", source=['tests/generate_image.cpp', 'tests/test-source-code/python-1.c']  + libitl_objs, target='generate-image')

    ctx(rule='${LD} ${SRC} -fPIC ${LDFLAGS} -shared -o ${TGT}', source=libitl_objs, target='libitl.so')
    ctx(rule='${AR} rvs  ${TGT} ${SRC}', source=[o for o in libitl_objs if not o.endswith('.a')], target='libitl.a')


    ctx.install_as('${PREFIX}/lib/libitl.so', 'libitl.so')
    ctx.install_as('${PREFIX}/lib/libitl.a', 'libitl.a')
    ctx.install_as('${PREFIX}/include/itl.h', 'src/itl.h')
    ctx.install_as('${PREFIX}/include/itl-code.hpp', 'src/itl-code.hpp')

    # Clean the files that weren't caught
    if ctx.cmd == "clean":
        ctx.exec_command('cd deps/freetype && make clean', shell = True)
        ctx.exec_command('cd deps/tree-sitter && make clean', shell = True)
    
    

    # ctx.(source=['tests/generate_image.cpp'], target='generate-image', use=['myobjects', 'deps/freetype/libfreetype.a', 'deps/harfbuzz/src/harfbuzz.o', 'deps/tree-sitter/libtree-sitter.a'])

    

def dist(ctx):
    ctx.algo = 'tar.gz'
    ctx.excl = '**/.git'
    
    
    


# Local Variables:
# mode: python
# End:

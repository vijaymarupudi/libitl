# libitl: A text to image library for eye tracking experiments

## Features

* Proportional fonts: People rarely read regular text with monospaced fonts. I think eye tracking experiments should not use monospaced text either.
* Precise locations for each rendered character.
* Semantic syntax highlighting powered by tree-sitter.

## Requirements

* A C and C++ compiler
* Nothing else, all other dependencies are vendored in.

## Compilation

```
git clone --recursive <url>
autoreconf -vif
./configure
make -j8
```




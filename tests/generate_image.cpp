#include <string.h>
#include <string>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "itl.h"
#include "itl-code.hpp"
#include "io-utils.h"
#include "test-source-code/test-source-code.h"

void code_rendering(ITLFontImageRendererCtx* ctx) {

  std::string code ((char *)test_source_code_python_1_py, test_source_code_python_1_py_len);

  auto highlights = itl_code_highlight(ITL_CODE_LANGUAGE_PYTHON, code);

  void* image_buffer;
  size_t image_buffer_len;

  itl_font_image_renderer_ctx_set_font_file(ctx, "fonts/NotoSansMono-Regular.ttf");
  itl_font_image_renderer_ctx_set_margin(ctx, 0);
  itl_font_image_renderer_ctx_set_line_spacing(ctx, 1.2);
  itl_font_image_renderer_ctx_set_font_size(ctx, 18);


  itl_font_image_renderer_ctx_set_bounding_box(ctx, true);
  itl_render_font_image(ctx,
                        code.c_str(), code.size(),
                        (ITLColorInterval*)highlights.data(),
                        highlights.size(),
                        &image_buffer,
                        &image_buffer_len,
                        NULL,
                        NULL);
  write_buffer_to_file(image_buffer, image_buffer_len, "tests-output/itl-test-output-1.png");
  free(image_buffer);

  itl_font_image_renderer_ctx_set_bounding_box(ctx, false);
  itl_render_font_image(ctx,
                        code.c_str(), code.size(),
                        (ITLColorInterval*)highlights.data(),
                        highlights.size(),
                        &image_buffer,
                        &image_buffer_len,
                        NULL,
                        NULL);
  write_buffer_to_file(image_buffer, image_buffer_len, "tests-output/itl-test-output-0.png");
  free(image_buffer);
}

int main(int argc, char **argv) {

  int err = mkdir("tests-output", 0755);

  if (err == EEXIST) {
    exit(1);
  }

  auto ctx = itl_font_image_renderer_ctx_new();
  auto text = "fi, fl, fi, ffi, ffl: This is a test!";

  itl_font_image_renderer_ctx_set_bounding_box(ctx, true);
  itl_render_font_image_to_file(ctx,
                                "fonts/Amiri-Regular.ttf",
                                "tests-output/itl-test-output.png",
                                text,
                                strlen(text),
                                0,
                                30);
  itl_font_image_renderer_ctx_set_bounding_box(ctx, false);

  code_rendering(ctx);



  itl_font_image_renderer_ctx_free(ctx);
}

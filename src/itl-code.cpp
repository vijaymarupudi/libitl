#include <tree_sitter/api.h>

#include <cassert>
#include <regex.h>
#include <cstring>
#include <unordered_map>

#include "itl-code.hpp"



extern "C" {
  TSLanguage *tree_sitter_python();
  extern char deps_tree_sitter_python_queries_highlights_scm[];
  extern unsigned int deps_tree_sitter_python_queries_highlights_scm_len;
}


struct Language {
  TSLanguage* (*language_constructor)();
  char* highlight_queries;
  unsigned int highlight_queries_len;
};

Language LANGUAGES[ITL_CODE_LANGUAGE_LENGTH] = {
  {
    tree_sitter_python,
    deps_tree_sitter_python_queries_highlights_scm,
    deps_tree_sitter_python_queries_highlights_scm_len
  }
};



static bool match_pattern(const std::string& code, TSNode node, const char* regex) {
  regex_t reg;

  regcomp(&reg, regex, REG_NOSUB);

  char buf[8128];
  size_t node_len = ts_node_end_byte(node) - ts_node_start_byte(node);
  memcpy(buf, code.c_str() + ts_node_start_byte(node), node_len);
  buf[node_len] = 0;



  bool result = regexec(&reg, buf, 0, NULL, 0) == 0;

  regfree(&reg);
  return result;
}


std::vector<ITLCodeSyntaxHighlightRegionsEntry> itl_code_highlight_regions(ITLCodeLanguage itl_lang, const std::string& code) {
  struct Language *lang = &LANGUAGES[itl_lang];

  TSParser *parser = ts_parser_new();

  TSLanguage *language = lang->language_constructor();
  ts_parser_set_language(parser, language);

  TSTree *tree = ts_parser_parse_string(
    parser,
    NULL,
    code.c_str(),
    code.size()
  );

  TSNode root_node = ts_tree_root_node(tree);

  uint32_t error_offset;
  TSQueryError error;
  TSQuery *query = ts_query_new(language, lang->highlight_queries,
                                lang->highlight_queries_len,
                                &error_offset, &error);

  TSQueryCursor *cursor = ts_query_cursor_new();
  ts_query_cursor_exec(cursor, query, root_node);

  TSQueryMatch match;
  uint32_t capture_index;

  std::vector<struct ITLCodeSyntaxHighlightRegionsEntry> entries;

  uint32_t prev_start = UINT32_MAX;
  uint32_t prev_end = UINT32_MAX;

  while (ts_query_cursor_next_capture(cursor, &match, &capture_index)) {
    TSNode node = match.captures[0].node;
    uint32_t start = ts_node_start_byte(node), end = ts_node_end_byte(node);

    uint32_t n_pred;
    const TSQueryPredicateStep *steps = ts_query_predicates_for_pattern(query, match.pattern_index, &n_pred);

    // #match query
    if (n_pred == 4) {
      uint32_t regex_len;
      const char* regex = ts_query_string_value_for_id(query, steps[2].value_id, &regex_len);
      if (!match_pattern(code, node, regex)) {
        continue;
      }
    }

    // for (unsigned int i = 0; i < n_pred; i++) {
    //   if(steps[i].type == TSQueryPredicateStepTypeString) {
    //     uint32_t string_len;
    //     printf("string: %s\n", ts_query_string_value_for_id(query, steps[i].value_id, &string_len));
    //   } else if (steps[i].type == TSQueryPredicateStepTypeDone) {
    //     printf("done\n");
    //   } else if (steps[i].type == TSQueryPredicateStepTypeCapture) {
    //     uint32_t string_len;
    //     printf("capture: %s\n", ts_query_capture_name_for_id(query, steps[i].value_id, &string_len));
    //   }
    // }

    if (prev_start == start && prev_end == end) { continue; }


    if (prev_end > start && prev_end != UINT32_MAX) {
      // overlapping match, then skip this match
      continue;
    } else if (prev_end != start && prev_end != UINT32_MAX) {
      // text that's not part of the ast (whitespace)
      entries.emplace_back("text", prev_end, start);
    }

    uint32_t capture_name_len;
    const char *capture_name = ts_query_capture_name_for_id(query, match.captures[0].index, &capture_name_len);

    entries.emplace_back(std::string(capture_name, capture_name_len), start, end);
    prev_start = start;
    prev_end = end;
  }


  if (prev_end != code.size() && prev_end != UINT32_MAX) {
    entries.emplace_back("text", prev_end, code.size());
  }

  ts_query_cursor_delete(cursor);
  ts_query_delete(query);
  ts_parser_delete(parser);

  return entries;

}


struct RGBColor {
    unsigned char r;
    unsigned char g;
    unsigned char b;
};

std::vector<ITLCodeSyntaxHighlightEntry> itl_code_highlight(ITLCodeLanguage lang, const std::string& code) {

  struct TreeSitterHighlightInfo
{
  char group[50];
  RGBColor color;
} highlight_info[] = {
    {"error", {255, 0, 0}},
    {"punctuation.bracket", {52, 52, 52}},
    {"punctuation.special", {52, 52, 52}},
    {"comment", {0, 0, 128}},
    {"constant", {121, 38, 94}},
    {"constant.builtin", {0, 255, 0}},
    {"constant.macro", {22, 93, 130}},
    {"string.regex", {199, 15, 46}},
    {"string", {199, 15, 46}},
    {"character", {199, 15, 46}},
    {"number", {9, 88, 134}},
    {"boolean", {0, 255, 0}},
    {"float", {9, 88, 134}},
    {"annotation", {121, 38, 94}},
    {"attribute", {22, 93, 130}},
    {"namespace", {22, 93, 130}},
    {"function.builtin", {121, 38, 94}},
    {"function", {121, 38, 94}},
    {"function.macro", {121, 38, 94}},
    {"parameter", {4, 165, 81}},
    {"parameter.reference", {4, 165, 81}},
    {"function.method", {121, 38, 94}},
    {"field", {4, 165, 81}},
    {"property", {4, 165, 81}},
    {"constructor", {22, 93, 130}},
    {"conditional", {175, 219, 0}},
    {"repeat", {175, 219, 0}},
    {"label", {4, 165, 81}},
    {"keyword", {175, 219, 0}},
    {"keyword.function", {0, 255, 0}},
    {"keyword.operator", {0, 255, 0}},
    {"operator", {52, 52, 52}},
    {"exception", {175, 219, 0}},
    {"type", {22, 93, 130}},
    {"type.builtin", {0, 255, 0}},
    {"structure", {4, 165, 81}},
    {"include", {175, 219, 0}},
    {"variable", {4, 165, 81}},
    {"variable.builtin", {4, 165, 81}},
    {"text", {52, 52, 52}},
    {"text.underline", {128, 0, 0}},
    {"tag", {0, 255, 0}},
    {"tag.delimiter", {0, 0, 0}},
    {"tag.attribute", {4, 165, 81}},
    {"text.title", {128, 0, 0}},
    {"text.literal", {52, 52, 52}},
    {"markdowntext.literal", {199, 15, 46}},
    {"markdown_inlinetext.literal", {199, 15, 46}},
    {"text.emphasis", {52, 52, 52}},
    {"text.strong", {0, 128, 0}},
    {"text.uri", {52, 52, 52}},
    {"textReference", {128, 0, 0}},
    {"punctuation.delimiter", {52, 52, 52}},
    {"stringEscape", {128, 0, 0}},
    {"text.note", {22, 93, 130}},
    {"text.warning", {128, 0, 0}}};

  size_t highlights_len = 56;


  std::unordered_map <std::string, RGBColor> color_map;
  for (unsigned i = 0; i < highlights_len; i++) {
    auto& info = highlight_info[i];
    color_map[std::string(info.group)] = info.color;
  }

  auto regions = itl_code_highlight_regions(lang, code);

  std::vector<ITLCodeSyntaxHighlightEntry> highlights;

  for (const auto& region : regions) {
    struct RGBColor color;
    auto it = color_map.find(region.name);
    if (it != color_map.end()) {
       color = it->second;
    } else {
      color = color_map["text"];
    }

    uint32_t rgba = 0xFF;
    rgba |= color.b << 8;
    rgba |= color.g << 16;
    rgba |= color.r << 24;

    highlights.push_back(ITLCodeSyntaxHighlightEntry{region.start, region.stop, rgba});

  }

  return highlights;

}

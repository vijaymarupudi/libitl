struct ImageColor {
  unsigned char red;
  unsigned char green;
  unsigned char blue;
  unsigned char alpha;
};

struct Image {
  ImageColor* buf;
  int width;
  int height;
  int buf_size;
};


struct ImageColor *image_buffer_get_buf_at_position(struct ImageColor *buf, int x, int y, int width) {
  return buf + (y * width) + x;
}

static ImageColor* image_get_buf_at_position(struct Image *image , int x, int y) {
  return image->buf + (y * image->width) + x;
}

static ImageColor* image_generate_buf(int width, int height, int *buf_size) {
  *buf_size = width * height * sizeof(ImageColor);
  return (ImageColor*)malloc(*buf_size);
}

static void image_line_set(struct Image *image, int x, int y, int width, struct ImageColor *color) {
  struct ImageColor *buf = image_get_buf_at_position(image, x, y);
  for (int j = 0; j < width; j++) {
    buf[j] = *color;
  }
}

static void image_rect(struct Image *image, int x, int y, int width, int height) {
  assert(x + width <= image->width);
  assert(y + height <= image->height);

  struct ImageColor black {0, 0, 0, 0xFF};

  for (int i = 0; i < height; i++) {
    if (i == 0) {
      image_line_set(image, x, y, width, &black);
    } else if (i == height - 1) {
      image_line_set(image, x, y + i, width, &black);
    } else {
      struct ImageColor *buf = image_get_buf_at_position(image, x, y + i);
      buf[0] = black;
      buf[width] = black;
    }
  }
}

static inline struct ImageColor image_color_blend(struct ImageColor dest, struct ImageColor src) {
  float factor = src.alpha / 255.;
  dest.red = dest.red * (1 - factor) + src.red * factor;
  dest.green = dest.green * (1 - factor) + src.green * factor;
  dest.blue = dest.blue * (1 - factor) + src.blue * factor;
  return dest;
}

static void image_add_to_buf(struct Image *image, int x, int y, struct ImageColor *input_buf,
                             int width, int height) {

  if (width == 0 || height == 0) {
    return;
  }

  int true_height = height;
  // if ((y + height) > iheight) {
  //   true_height = iheight - y;
  // }

  int true_width = width;
  // if (((x + width)) > iwidth) {
  //   true_width = iwidth - x;
  // }

  // printf("(%d, %d) : bitmap=(%d, %d) : image=(%d, %d) : final_bitmap=(%d, %d)\n", x, y, width, height, iwidth, iheight, true_width, true_height);

  // Check if it'll fit
  // assert((y + height) <= iheight);
  // assert((x + width) <= iwidth);


  for (int h = 0; h < true_height; h++) {
    struct ImageColor *b_dest = image_get_buf_at_position(image, x, y + h);
    struct ImageColor *b_src = image_buffer_get_buf_at_position(input_buf, 0, h, width);
    for (int i = 0; i < true_width; i++) {
      b_dest[i] = image_color_blend(b_dest[i], b_src[i]);
    }
  }
}


static void image_add_to_buf_mono(struct Image *image, int x, int y, unsigned char *input_buf,
                                  int width, int height, struct ImageColor color) {

  if (width == 0 || height == 0) {
    return;
  }

  int true_height = height;
  // if ((y + height) > iheight) {
  //   true_height = iheight - y;
  // }

  int true_width = width;
  // if (((x + width)) > iwidth) {
  //   true_width = iwidth - x;
  // }

  // printf("(%d, %d) : bitmap=(%d, %d) : image=(%d, %d) : final_bitmap=(%d, %d)\n", x, y, width, height, iwidth, iheight, true_width, true_height);

  // Check if it'll fit
  // assert((y + height) <= iheight);
  // assert((x + width) <= iwidth);


  for (int h = 0; h < true_height; h++) {
    struct ImageColor *b_dest = image_get_buf_at_position(image, x, y + h);
    unsigned char *b_src = input_buf + h * width;
    for (int i = 0; i < true_width; i++) {
      ImageColor to_blend = color;
      to_blend.alpha = b_src[i];
      b_dest[i] = image_color_blend(b_dest[i], to_blend);
    }
  }
}

static void image_deinit(struct Image *image) {
  free(image->buf);
}

static void image_invert(struct Image *image) {
  for (int i = 0; i < image->buf_size; i++) {
    struct ImageColor *pixel = &image->buf[i];
    pixel->red = 255 - pixel->red;
    pixel->green = 255 - pixel->green;
    pixel->blue = 255 - pixel->blue;
  }
}

static void image_init(struct Image *image, int width, int height) {
  int buf_size;
  struct ImageColor* buf = image_generate_buf(width, height, &buf_size);
  memset(buf, 0xFF, buf_size);
  image->buf = buf;
  image->width = width;
  image->height = height;
  image->buf_size = buf_size;
}

#pragma once

#include <stdint.h>
#include <string>
#include <vector>

struct ITLCodeSyntaxHighlightRegionsEntry {
  std::string name;
  uint32_t start;
  uint32_t stop;
  ITLCodeSyntaxHighlightRegionsEntry(std::string&& name, uint32_t start, uint32_t stop) {
    this->name = std::move(name);
    this->start = start;
    this->stop = stop;
  }
};

struct ITLCodeSyntaxHighlightEntry {
  uint32_t start;
  uint32_t stop;
  uint32_t rgba;
};

enum ITLCodeLanguage {
  ITL_CODE_LANGUAGE_PYTHON = 0,
  ITL_CODE_LANGUAGE_LENGTH
};


std::vector<ITLCodeSyntaxHighlightRegionsEntry> itl_code_highlight_regions(ITLCodeLanguage itl_lang, const std::string& code);

std::vector<ITLCodeSyntaxHighlightEntry> itl_code_highlight(ITLCodeLanguage lang, const std::string& code);

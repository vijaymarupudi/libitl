#include <string>
#include <inttypes.h>

static inline int utf8_next_char_byte_index(const std::string& str, int current_idx) {
  int idx = current_idx + 1;
  for (;idx < str.size() && (((unsigned char)str[idx]) & 0b11000000) == 0b10000000; idx++) {}
  return idx;
}

static inline uint32_t utf8_codepoint_at_index(const std::string& str, int current_idx) {
  
  int end_index = utf8_next_char_byte_index(str, current_idx);
  int char_len = end_index - current_idx;
  uint32_t val = 0;

  switch (char_len) {
  case 1: return str[current_idx];
  case 2:
    val |= ((0b11111) & str[current_idx]) << 6;
    val |= ((0b111111) & str[current_idx + 1]);
    return val;
  case 3:
    val |= ((0b1111) & str[current_idx]) << 12;
    val |= ((0b111111) & str[current_idx + 1]) << 6;
    val |= ((0b111111) & str[current_idx + 2]);
    return val;
  case 4:
    val |= ((0b111) | str[current_idx]) << 18;
    val |= ((0b111111) & str[current_idx + 1]) << 12;
    val |= ((0b111111) & str[current_idx + 2]) << 6;
    val |= ((0b111111) & str[current_idx + 3]) << 0;
    return val;
  }
  return val;
}

static inline int utf8_strlen(const std::string& str) {
  int idx = 0;
  int count = 0;
  while (idx < str.size()) {
    idx = utf8_next_char_byte_index(str, idx);
    count++;
  }
  return count;
}

static inline int utf8_strlen(const std::string& str, int start, int end) {
  int idx = start;
  int count = 0;
  while (idx < end) {
    idx = utf8_next_char_byte_index(str, idx);
    count++;
  }
  return count;
}

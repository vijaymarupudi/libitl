#include "itl.h"

#include <stddef.h>
#include <stdbool.h>
#include <inttypes.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <assert.h>

#include <vector>
#include <string>
#include <unordered_map>
#include <algorithm>
#include <utility>


#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H

#include <hb.h>
#include <hb-ft.h>
#include <hb-ot.h>
#include "nlohmann/json.hpp"


#include "noto-serif.h"
#include "stb_image_write.h"
#include "utf8-utils.hpp"
#include "image.hpp"
#include "io-utils.h"


template <class... T>
static void log(const char* temp, T... args) {
  if (false) { printf("log: "); printf(temp, args...); }
}

using json = nlohmann::json;


struct ITLFontImageRendererCtx {
  FT_Library ftlibrary;
  uint32_t margin;
  uint32_t font_size;
  float line_spacing;
  _Bool bounding_box;
  FT_Face ft_face;
  hb_font_t* hb_font;
  void* font_buffer;
  size_t font_buffer_len;
  _Bool debug;
};

static void itl_font_image_renderer_ctx_init(struct ITLFontImageRendererCtx *ctx) {
  FT_Error error;
  error = FT_Init_FreeType(&ctx->ftlibrary);
  if (error) {
    abort();
  }
  ctx->margin = 0;
  ctx->font_size = 18;
  ctx->line_spacing = 1.0;
  ctx->bounding_box = false;
  ctx->ft_face = NULL;
  ctx->hb_font = NULL;
  ctx->debug = false;
}


static void itl_font_image_renderer_ctx_deinit_fonts(struct ITLFontImageRendererCtx *ctx) {
  FT_Error error;

  if (!ctx->hb_font) { return; }

  log("freeing fonts\n");

  if (ctx->hb_font) {
    hb_font_destroy(ctx->hb_font);

  }

  if (ctx->ft_face) {
    error = FT_Done_Face(ctx->ft_face);
    if (error) {
      abort();
    }

  }

  if (ctx->font_buffer) {
    free(ctx->font_buffer);
  }

  ctx->font_buffer = NULL;
  ctx->font_buffer_len = 0;
  ctx->ft_face = NULL;
  ctx->hb_font = NULL;

}

static void itl_font_image_renderer_ctx_deinit(struct ITLFontImageRendererCtx *ctx) {
  FT_Error error;

  itl_font_image_renderer_ctx_deinit_fonts(ctx);

  log("freeing ftlibrary\n");

  error = FT_Done_FreeType(ctx->ftlibrary);
  if (error) {
    abort();
  }


}



extern "C" {

  struct ITLFontImageRendererCtx * itl_font_image_renderer_ctx_new() {
    struct ITLFontImageRendererCtx* ptr = (struct ITLFontImageRendererCtx*) malloc(sizeof(struct ITLFontImageRendererCtx));
    itl_font_image_renderer_ctx_init(ptr);
    return ptr;
  }

  void itl_font_image_renderer_ctx_free(struct ITLFontImageRendererCtx *ctx) {
    itl_font_image_renderer_ctx_deinit(ctx);
    free(ctx);
  }





  void itl_font_image_renderer_ctx_set_margin(ITLFontImageRendererCtx* ctx, uint32_t margin) {
    ctx->margin = margin;
  }

  void itl_font_image_renderer_ctx_set_font_size(ITLFontImageRendererCtx* ctx, uint32_t font_size) {
    bool changed = false;

    if (ctx->font_size != font_size) {
      changed = true;
    }

    ctx->font_size = font_size;

    if (changed && ctx->ft_face) {
      log("changing font size on fonts\n");
      FT_Set_Pixel_Sizes(ctx->ft_face, 0, ctx->font_size);
      hb_ft_font_changed(ctx->hb_font);
    }

  }

  ITLError itl_font_image_renderer_ctx_set_font(ITLFontImageRendererCtx* ctx, void* font_buffer, size_t font_buffer_len) {
    FT_Error error;
    FT_Face face;

    itl_font_image_renderer_ctx_deinit_fonts(ctx);

    ctx->font_buffer = malloc(font_buffer_len);
    ctx->font_buffer_len = font_buffer_len;
    memcpy(ctx->font_buffer, font_buffer, font_buffer_len);

    error = FT_New_Memory_Face(ctx->ftlibrary, (const FT_Byte*)ctx->font_buffer, ctx->font_buffer_len, 0, &face);
    if (error) {
      free(ctx->font_buffer);
      return ITL_ERROR_FREETYPE;
    } else {
      log("initializing fonts\n");
      ctx->ft_face = face;
      FT_Set_Pixel_Sizes(ctx->ft_face, 0, ctx->font_size);
      ctx->hb_font = hb_ft_font_create(ctx->ft_face, NULL);

      return ITL_OK;
    }
  }

  ITLError itl_font_image_renderer_ctx_set_font_file(ITLFontImageRendererCtx* ctx, const char* path) {
    size_t len;
    void* buf = read_buffer_from_file(path, &len);
    ITLError err = itl_font_image_renderer_ctx_set_font(ctx, buf, len);
    free(buf);
    return err;
  }

  void itl_font_image_renderer_ctx_set_line_spacing(ITLFontImageRendererCtx* ctx, float line_spacing) {
    ctx->line_spacing = line_spacing;
  }

  void itl_font_image_renderer_ctx_set_bounding_box(ITLFontImageRendererCtx* ctx, _Bool bounding_box) {
    ctx->bounding_box = bounding_box;
  }


}

static void itl_font_image_renderer_ctx_ensure_font_inited(struct ITLFontImageRendererCtx *ctx) {
  if (!ctx->ft_face) {
    log("using default noto serif font\n");
    itl_font_image_renderer_ctx_set_font(ctx, itl_font_data_noto_serif, itl_font_data_noto_serif_len);
  }
}





static int validate_color_intervals(ITLColorInterval *intervals, size_t len, size_t text_byte_len) {
  if (len != 0) {
    ITLColorInterval first = intervals[0];
    ITLColorInterval last = intervals[len - 1];
    if (!(first.start == 0 && last.stop == text_byte_len)) { return 1; }


    for (int i = 1; i < len; i++) {
      ITLColorInterval prev = intervals[i - 1];
      ITLColorInterval cur = intervals[i];
      if (prev.stop != cur.start) { return 1; }
    }
  }
  return 0;
}

#define FT_ERROR_PANIC(ft_error) if (ft_error) {printf("FT_Error: %s\n", FT_Error_String(ft_error)); exit(1);}

struct ITLCharInfo {
  // glyph information
  uint32_t glyph_id;
  uint32_t glyph_cluster;
  int32_t glyph_x;
  int32_t glyph_y;
  uint32_t glyph_width;
  uint32_t glyph_height;
  bool is_glyph_ligature;
  uint8_t glyph_number_of_characters;

  // shaping info
  // uint32_t shaping_x;
  // uint32_t shaping_y;

  // char info
  uint32_t char_byte_index;
  uint32_t char_index;
  uint8_t glyph_char_index;
  uint32_t unicode_codepoint;
  char char_string[5];
  int32_t char_x;
  int32_t char_y;
  uint32_t char_width;
  uint32_t char_height;
  int32_t char_centroid_x;
  int32_t char_centroid_y;
};


static json itl_char_info_to_json (const ITLCharInfo& info) {
  json obj = json::object();
  obj["glyph_id"] = info.glyph_id;
  obj["glyph_cluster"] = info.glyph_cluster;
  obj["glyph_x"] = info.glyph_x;
  obj["glyph_y"] = info.glyph_y;
  obj["glyph_width"] = info.glyph_width;
  obj["glyph_height"] = info.glyph_height;
  obj["is_glyph_ligature"] = info.is_glyph_ligature;
  obj["glyph_number_of_characters"] = info.glyph_number_of_characters;
  // char info
  obj["char_byte_index"] = info.char_byte_index;
  obj["char_index"] = info.char_index;
  obj["glyph_char_index"] = info.glyph_char_index;
  obj["unicode_codepoint"] = info.unicode_codepoint;
  obj["char_string"] = info.char_string;
  obj["char_x"] = info.char_x;
  obj["char_y"] = info.char_y;
  obj["char_width"] = info.char_width;
  obj["char_height"] = info.char_height;
  obj["char_centroid_x"] = info.char_centroid_x;
  obj["char_centroid_y"] = info.char_centroid_y;
  return obj;
}


static void itl_char_info_log(const ITLCharInfo& info) {

  log(("CharInfo:\n"
          "  glyph_id=%u\n"
          "  glyph_cluster=%u\n"
          "  glyph_x=%d\n"
          "  glyph_y=%d\n"
          "  glyph_width=%u\n"
          "  glyph_height=%u\n"
          "  is_glyph_ligature=%d\n"
          "  glyph_number_of_characters=%u\n"
          "  char_byte_index=%u\n"
          "  char_index=%u\n"
          "  glyph_char_index=%u\n"
          "  unicode_codepoint=%u\n"
          "  char_string='%s'\n"
          "  char_x=%d\n"
          "  char_y=%d\n"
          "  char_width=%u\n"
          "  char_height=%u\n"
          "  char_centroid_x=%d\n"
          "  char_centroid_y=%d\n"),
         info.glyph_id,
         info.glyph_cluster,
         info.glyph_x,
         info.glyph_y,
         info.glyph_width,
         info.glyph_height,
         info.is_glyph_ligature,
         info.glyph_number_of_characters,

         // char info
         info.char_byte_index,
         info.char_index,
         info.glyph_char_index,
         info.unicode_codepoint,
         info.char_string,
         info.char_x,
         info.char_y,
         info.char_width,
         info.char_height,
         info.char_centroid_x,
         info.char_centroid_y
         );
}

struct ITLGlyphCache;

struct ITLRenderContext {
  std::string text;
  ITLColorInterval *color_intervals;
  size_t color_intervals_len;

  size_t color_intervals_index = 0;

  std::vector<ITLCharInfo> info;
  uint32_t byte_index = 0;
  uint32_t char_index = 0;

  ITLRenderContext(std::string text, ITLColorInterval *color_intervals, size_t color_intervals_len) {
    this->text = std::move(text);
    this->color_intervals = color_intervals;
    this->color_intervals_len = color_intervals_len;
  }

  struct ImageColor next_char_color() {
    if (color_intervals_len == 0) {
      // black
      return (struct ImageColor) {0, 0, 0, 0xFF};
    }

    for (int i = color_intervals_index; i < color_intervals_len; i++) {
      ITLColorInterval interval = color_intervals[i];
      if (byte_index >= interval.start && byte_index < interval.stop) {
        unsigned char r = (interval.rgba & 0xFF000000) >> 24;
        unsigned char g = (interval.rgba & 0x00FF0000) >> 16;
        unsigned char b = (interval.rgba & 0x0000FF00) >> 8;
        color_intervals_index = i;
        return (struct ImageColor) {r, g, b, 0xFF};
      }
    }
    abort();
  }

  void newline() {
    byte_index++;
    char_index++;
  }

  void add_information(ITLGlyphCache& cache, int32_t x, int32_t y, uint8_t glyph_n_chars, uint32_t cluster, uint32_t glyph_id);


  json info_json() {
    auto arr = json::array();
    for (const auto& info_item : this->info) {
      arr.push_back(itl_char_info_to_json(info_item));
    }
    return arr;
  }

};



struct ITLLine {
  std::vector<hb_codepoint_t> glyph_ids;
  std::vector<hb_glyph_position_t> positions;
  std::vector<unsigned int> clusters;
  std::string line_text;
  int width;
  int bitmap_top;
  int bitmap_bottom;
  int bitmap_left;
  void add_glyph_info(hb_glyph_info_t& glyph_info, const hb_glyph_position_t& position);
  void compile(ITLGlyphCache& cache);
};



struct ITLGlyphCacheItem {
private:
  std::vector<hb_position_t> endings;
public:
  FT_BitmapGlyph glyph;


  std::vector<hb_position_t>& get_glyph_endings(unsigned int n_chars) {
    if (endings.size() > 0) {
      return endings;
    }

    unsigned int width = glyph->left + glyph->bitmap.width;
    unsigned int step = width / n_chars;

    for (unsigned int i = 1; i < n_chars + 1; i++) {
      endings.push_back(i * step);
    }

    // in case integer division leads to imprecision
    endings[endings.size()] = width;

    return endings;

  }

  ITLGlyphCacheItem(std::vector<hb_position_t> carets, FT_BitmapGlyph glyph) {
    this->glyph = glyph;
    this->endings = std::move(carets);

    // carets only have n-1 items, the points between a ligature. I'm adding the last ending.

    if (this->endings.size() != 0) {
      hb_position_t ending_pos = glyph->left + glyph->bitmap.width;
      this->endings.push_back(ending_pos);
    }

  }

  ITLGlyphCacheItem(const ITLGlyphCacheItem& other) = delete;
  ITLGlyphCacheItem(ITLGlyphCacheItem&& other) {
    this->glyph = other.glyph;
    other.glyph = NULL;
  }
  ~ITLGlyphCacheItem();
};

ITLGlyphCacheItem::~ITLGlyphCacheItem() {
  log("deleting glyph\n");
  if (glyph) {
    FT_Done_Glyph(reinterpret_cast<FT_Glyph>(glyph));
  }
}


struct ITLGlyphCache {
  FT_Face face;
  hb_font_t* hb_font;
  std::unordered_map<hb_codepoint_t, ITLGlyphCacheItem> ht;
  ITLGlyphCacheItem& get_glyph_info(hb_codepoint_t glyph_id);
};


ITLGlyphCacheItem& ITLGlyphCache::get_glyph_info(hb_codepoint_t glyph_id) {
  auto it = ht.find(glyph_id);
  if (it == ht.end()) {
    FT_Error error;
    error = FT_Load_Glyph(face, glyph_id, 0);
    FT_ERROR_PANIC(error);
    FT_Glyph glyph;
    error = FT_Get_Glyph(face->glyph, &glyph);
    FT_ERROR_PANIC(error);
    error = FT_Glyph_To_Bitmap(&glyph, FT_RENDER_MODE_NORMAL, 0, true);
    FT_ERROR_PANIC(error);

    FT_BitmapGlyph bitmap_glyph = reinterpret_cast<FT_BitmapGlyph>(glyph);

    log("glyph rendered: left=%d, top=%d, height=%d, width=%d\n", bitmap_glyph->left,
        bitmap_glyph->top,
        bitmap_glyph->bitmap.rows,
        bitmap_glyph->bitmap.width);

    hb_position_t caret_array[100];
    unsigned int caret_count = 100;
    unsigned int n = hb_ot_layout_get_ligature_carets(this->hb_font,
                                                      HB_DIRECTION_LTR,
                                                      glyph_id,
                                                      0, &caret_count, caret_array);

    // if this vector is zero, then the glyph's number of characters
    // will be determined during render time
    std::vector<hb_position_t> carets;

    if (caret_count) {
      log("ligature carets found\n");
      for (int i = 0; i < caret_count; i++) {
        carets.push_back(caret_array[i] / 64);
      }
    }

    ht.emplace(glyph_id, ITLGlyphCacheItem {std::move(carets), bitmap_glyph});


  }

  it = ht.find(glyph_id);
  return it->second;
}

void ITLRenderContext::add_information(ITLGlyphCache& cache, int32_t x, int32_t y, uint8_t glyph_n_chars, uint32_t cluster, uint32_t glyph_id) {


  ITLGlyphCacheItem& cache_item = cache.get_glyph_info(glyph_id);
  FT_BitmapGlyph& glyph = cache_item.glyph;
  auto& char_endings = cache_item.get_glyph_endings(glyph_n_chars);

  log("glyph retrieved: left=%d, top=%d, height=%d, width=%d\n", glyph->left,
        glyph->top,
        glyph->bitmap.rows,
        glyph->bitmap.width);

  int32_t glyph_x = x + glyph->left;
  int32_t glyph_y = y - glyph->top;
  uint32_t glyph_width = glyph->bitmap.width;
  uint32_t glyph_height = glyph->bitmap.rows;

  int width_per_character = glyph_width / glyph_n_chars;

  int32_t char_y = y - glyph->top;

  uint8_t glyph_char_i = 0;
  for (int32_t char_x = x; glyph_char_i < char_endings.size(); glyph_char_i++) {

    unsigned int ending_pos = x + char_endings[glyph_char_i];
    uint32_t char_width = ending_pos - char_x;
    uint32_t char_height = glyph_height;
    uint32_t codepoint = utf8_codepoint_at_index(text, byte_index);
    char char_buf[5] = {0};
    uint32_t next_byte_index = utf8_next_char_byte_index(text, byte_index);

    for (uint32_t b = byte_index, i = 0; b < next_byte_index; b++, i++) {
      char_buf[i] = text[b];
    }

    ITLCharInfo char_info = {
      glyph_id, cluster, glyph_x, glyph_y, glyph_width, glyph_height,
      glyph_n_chars != 1, glyph_n_chars,

      byte_index, char_index, glyph_char_i, codepoint, {0},
      char_x, char_y, char_width, char_height,
      static_cast<int32_t>(char_x + char_width / 2),
      static_cast<int32_t>(char_y + char_height / 2)
    };

    memcpy(char_info.char_string, char_buf, 5);

    itl_char_info_log(char_info);
    this->info.push_back(std::move(char_info));

    byte_index = next_byte_index;
    char_index += 1;
    char_x = ending_pos;
  }

}



void ITLLine::add_glyph_info(hb_glyph_info_t& glyph_info, const hb_glyph_position_t& position)  {
  glyph_ids.push_back(glyph_info.codepoint);
  clusters.push_back(glyph_info.cluster);
  positions.push_back(position);
}

void ITLLine::compile(ITLGlyphCache& cache) {

  int top = 0;
  int bottom = 0;

  int width = 0;
  for (int i = 0; i < glyph_ids.size(); i++) {
    FT_BitmapGlyph& glyph = cache.get_glyph_info(glyph_ids[i]).glyph;
    top = std::max(glyph->top, top);
    bottom = std::max(-glyph->top + (int)glyph->bitmap.rows, bottom);
    if (i != glyph_ids.size() - 1) {
      width += positions[i].x_advance / 64;
    }
  }

  this->width = width;
  this->bitmap_top = top;
  this->bitmap_bottom = bottom;


  if (glyph_ids.size() > 0) {
    this->bitmap_left = cache.get_glyph_info(glyph_ids[0]).glyph->left;
    FT_BitmapGlyph& last_glyph = cache.get_glyph_info(glyph_ids[glyph_ids.size() - 1]).glyph;
    this->width += last_glyph->left + (int)last_glyph->bitmap.width;
  }
}

struct Stimulus {
  std::vector<ITLLine> lines;
  uint32_t margin;
  float line_spacing;
  uint32_t font_size;
  int width;
  int height;
  int bitmap_left;

  void compile(ITLGlyphCache& cache);
  void starting_cursor_location(int& x, int& y);
  // json get_render_info(ITLRenderContext& context);
};

void Stimulus::starting_cursor_location(int& x, int& y) {
  x = margin - bitmap_left;
  y = margin;
  if (lines.size()) {
    y += lines[0].bitmap_top;
  }
}

void Stimulus::compile(ITLGlyphCache& cache) {
  int width = 0;
  bitmap_left = 0;

  for (int i = 0; i < lines.size(); i++) {
    ITLLine& line = lines[i];
    line.compile(cache);
    width = std::max(width, line.width);
    bitmap_left = std::min(bitmap_left, line.bitmap_left);
  }

  width = width +
    -bitmap_left +
    2 * margin;

  int height = 2 * margin;

  if (lines.size() != 0) {
    int line_space = (lines.size() - 1) * line_spacing * font_size;
    height += line_space + lines[0].bitmap_top + (lines[lines.size() - 1].bitmap_bottom);
  }

  this->width = width;
  this->height = height;

  int x, y;
  this->starting_cursor_location(x, y);

  const char log_string[] = ("Stimulus:\n  width=%d"
                             "\n  height=%d"
                             "\n  margin=%d"
                             "\n  starting x=%d, y=%d"
                             "\n  bitmap_left=%d\n");
  log(log_string,
      this->width,
      this->height,
      this->margin,
      x, y,
      this->bitmap_left
      );
}


static inline void image_draw_ft_bitmap(struct Image *image, FT_Bitmap *bitmap, int x, int y, struct ImageColor color) {
  image_add_to_buf_mono(image, x, y, (unsigned char*)bitmap->buffer,
                   bitmap->width, bitmap->rows, color);
}


void itl_line_render(ITLLine& line, struct Image& image, ITLGlyphCache& cache, int x, int y, ITLRenderContext& render_context) {


  const auto& text = line.line_text;
  int text_len = utf8_strlen(text);
  size_t text_size = text.size();

  int byte_idx = 0;
  int char_idx = 0;

  for (int i = 0; i < line.glyph_ids.size(); i++) {
    auto& pos = line.positions[i];
    auto glyph_id = line.glyph_ids[i];
    unsigned int cluster = line.clusters[i];
    int glyph_n_chars = i == line.glyph_ids.size() - 1 ? utf8_strlen(text, byte_idx, text_size) :
      utf8_strlen(text, byte_idx, line.clusters[i+1]);

    struct ImageColor color = render_context.next_char_color();


    ITLGlyphCacheItem& info = cache.get_glyph_info(glyph_id);
    int xpos = x + (pos.x_offset / 64) + info.glyph->left;
    int ypos = y - (pos.y_offset / 64) - info.glyph->top;
    // printf("%d, %d, %d, %d\n", xpos, ypos, (pos.y_offset / 64), info.glyph->top);
    image_draw_ft_bitmap(&image, &info.glyph->bitmap, xpos, ypos, color);


    render_context.add_information(cache, x, y, glyph_n_chars, cluster, glyph_id);

    for (int j = 0; j < glyph_n_chars; j++) {
      byte_idx = utf8_next_char_byte_index(text, byte_idx);
    }

    char_idx += glyph_n_chars;


    x += pos.x_advance / 64;
    y += pos.y_advance / 64;
  }

}

static void itl_stimulus_render(Stimulus& stim, Image& image, ITLGlyphCache& cache, ITLRenderContext& render_context) {
  int x, y;
  stim.starting_cursor_location(x, y);

  for (auto& line : stim.lines) {
    itl_line_render(line, image, cache, x, y, render_context);
    y += (int)(stim.font_size * stim.line_spacing);
    render_context.newline();
  }
}


static std::vector<std::string> split(std::string& str, std::string& delim) {
  std::vector<std::string> items;
  int idx = 0;
  int new_pos;
  while (idx < str.size() && (new_pos = str.find(delim, idx)) != std::string::npos) {
    items.push_back(str.substr(idx, new_pos - idx));
    idx = new_pos + 1;
  }

  if (idx != str.size()) {
    items.push_back(str.substr(idx, str.size() - idx));
  }

  return items;
}


extern "C" unsigned char *stbi_write_png_to_mem(const unsigned char *pixels, int stride_bytes, int x, int y, int n, size_t *out_len);

extern "C" ITLError itl_render_font_image(struct ITLFontImageRendererCtx *ctx,
                                          const char* text_input,
                                          size_t text_input_len,
                                          ITLColorInterval *color_intervals,
                                          size_t color_intervals_len,
                                          void** out_image_buffer,
                                          size_t* out_image_buffer_len,
                                          char** out_render_info,
                                          size_t* out_render_info_len) {

  ITLError ret_code = ITL_OK;

  if (validate_color_intervals(color_intervals, color_intervals_len, text_input_len)) {
    return ITL_ERROR_COLOR_INTERVALS;
  }

  itl_font_image_renderer_ctx_ensure_font_inited(ctx);

  hb_buffer_t *buf;
  buf = hb_buffer_create();

  ITLGlyphCache cache {ctx->ft_face, ctx->hb_font};

  std::string text = std::string(text_input, text_input_len);

  log("strlen: %d, size: %lu\n", utf8_strlen(text), text.size());

  std::vector<ITLLine> lines;

  std::string delim ("\n");

  auto line_strings = split(text, delim);

  // printf("%s\n", font_filename);

  for (auto&& line_str : line_strings) {
    hb_buffer_set_direction(buf, HB_DIRECTION_LTR);
    hb_buffer_set_script(buf, HB_SCRIPT_LATIN);
    hb_buffer_set_language(buf, hb_language_from_string("en", -1));
    hb_buffer_add_utf8(buf, line_str.c_str(), line_str.size(), 0, line_str.size());
    hb_shape(ctx->hb_font, buf, NULL, 0);

    unsigned int glyph_count;
    hb_glyph_info_t *glyph_info    = hb_buffer_get_glyph_infos(buf, &glyph_count);
    hb_glyph_position_t *glyph_pos = hb_buffer_get_glyph_positions(buf, &glyph_count);

    auto line_tmp = ITLLine{};

    lines.push_back(std::move(line_tmp));

    auto& line = lines[lines.size() - 1];

    line.line_text = std::move(line_str);

    for (unsigned int i = 0; i < glyph_count; i++) {

      // Here was an attempt to get ligature carets. Testing with
      // latin fonts printed nothing. Leaving here for another try
      // (and maybe another language). It's possible latin fonts don't
      // include this information.

      hb_position_t caret_array[100];
      unsigned int caret_count = 100;
      unsigned int n = hb_ot_layout_get_ligature_carets(ctx->hb_font,
                                                        HB_DIRECTION_LTR,
                                                        glyph_info[i].codepoint,
                                                        0, &caret_count, caret_array);

      line.add_glyph_info(glyph_info[i], glyph_pos[i]);
    }

    hb_buffer_reset(buf);
  }

  Stimulus stimulus { std::move(lines), ctx->margin, ctx->line_spacing, ctx->font_size };
  stimulus.compile(cache);


  struct Image image;
  image_init(&image, stimulus.width, stimulus.height);

  ITLRenderContext render_context {text, color_intervals, color_intervals_len};

  itl_stimulus_render(stimulus, image, cache, render_context);

  if (ctx->bounding_box) {
    for (const auto& info : render_context.info) {
      image_rect(&image, info.char_x, info.char_y, info.char_width, info.char_height);
    }
  }

  if (out_render_info) {
    auto json_string = render_context.info_json().dump();
    char* buf = (char*)malloc(json_string.size() + 1);
    buf[json_string.size()] = 0;
    memcpy(buf, &json_string[0], json_string.size());
    *out_render_info = buf;
    *out_render_info_len = json_string.size();
  }

  *out_image_buffer_len = 0;

  *out_image_buffer = stbi_write_png_to_mem((unsigned char *)image.buf, image.width * 4, image.width, image.height, 4, out_image_buffer_len);
  image_deinit(&image);

  hb_buffer_destroy(buf);
  return ret_code;
}


extern "C" void itl_render_font_image_to_file(struct ITLFontImageRendererCtx *ctx,
                                              const char* font_filename,
                                              const char* out_filename,
                                              const char* text,
                                              size_t text_len,
                                              uint32_t margin,
                                              uint32_t font_size) {

  itl_font_image_renderer_ctx_set_font_file(ctx, font_filename);
  itl_font_image_renderer_ctx_set_font_size(ctx, font_size);

  void* image_buffer;
  size_t image_buffer_len;


  itl_render_font_image(ctx,
                        text,
                        text_len,
                        NULL,
                        0,
                        &image_buffer,
                        &image_buffer_len,
                        NULL,
                        NULL);

  write_buffer_to_file(image_buffer, image_buffer_len, out_filename);
  free(image_buffer);
}

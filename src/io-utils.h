#pragma once

static void* read_buffer_from_file(const char *filename, size_t *length) {

  void *source = NULL;
  size_t newLen;
  FILE *fp = fopen(filename, "r");
  if (fp != NULL) {
    /* Go to the end of the file. */
    if (fseek(fp, 0L, SEEK_END) == 0) {
      /* Get the size of the file. */
      long bufsize = ftell(fp);
      if (bufsize == -1) { /* Error */ }

      /* Allocate our buffer to that size. */
      source = malloc(sizeof(char) * bufsize);

      /* Go back to the start of the file. */
      if (fseek(fp, 0L, SEEK_SET) != 0) { /* Error */ }

      /* Read the entire file into memory. */
      newLen = fread(source, sizeof(char), bufsize, fp);
    }
    fclose(fp);
  }

  *length = newLen;
  return source;

}

static void write_buffer_to_file(const void* buffer, size_t buffer_len, const char* filename) {
  int fd = open(filename, O_RDWR | O_CREAT, 0644);
  write(fd, buffer, buffer_len);
  close(fd);
}

(use-modules (json))

(define colorscheme-data (let* ((data (call-with-input-file "vscode-light.scm" read))
                                (colors (assoc-ref data "colors"))
                                (groups (assoc-ref data "groups")))
                           (map
                            (lambda (pair)
                              (let ((group-name (car pair))
                                    (color-name (cdr pair)))
                                (cons group-name (assoc-ref colors color-name))))
                            groups)))

(display (let* ((re (make-regexp "[.@]"))
                (color-items (map
                              (lambda (pair)
                                (let* ((group-name (car pair))
                                       (color-hex-string (cdr pair))
                                       (color-number (string->number (substring color-hex-string 1) 16))
                                       (r (ash (logand color-number #xFF0000) -16))
                                       (g (ash (logand color-number #x00FF00) -8))
                                       (b (ash (logand color-number #x0000FF) 0))
                                       (rs (number->string r))
                                       (gs (number->string g))
                                       (bs (number->string b)))
                                  
                                  ;; (pk (regexp-substitute/global #f re (if (char=? (string-ref group-name 0) #\@)
                                  ;;                                         (substring group-name 1)
                                  ;;                                         group-name)
                                  ;;                               'pre "_" 'post))

                                  (string-append "{\"" group-name "\"" ", {" rs ", " bs ", " gs "}}")
                                  
                                  ))
                              colorscheme-data))
                )
           (string-append "struct RGBColor {
  unsigned char r;
  unsigned char g;
  unsigned char b;
};

struct TreeSitterHighlightInfo
{
  char group[50];
  struct RGBColor color;
} highlight_info[] = {
"
                          (string-join (map (lambda (str) (string-append "  " str)) color-items) "\n")
                          "};")
           ))

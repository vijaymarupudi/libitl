(("groups" . (("@error" . "vscRed")
              ("@punctuation.bracket" . "vscFront")
              ("@punctuation.special" . "vscFront")
              ("@comment" . "vscGreen")
              ("@constant" . "vscYellow")
              ("@constant.builtin" . "vscBlue")
              ("@constant.macro" . "vscBlueGreen")
              ("@string.regex" . "vscOrange")
              ("@string" . "vscOrange")
              ("@character" . "vscOrange")
              ("@number" . "vscLightGreen")
              ("@boolean" . "vscBlue")
              ("@float" . "vscLightGreen")
              ("@annotation" . "vscYellow")
              ("@attribute" . "vscBlueGreen")
              ("@namespace" . "vscBlueGreen")
              ("@function.builtin" . "vscYellow")
              ("@function" . "vscYellow")
              ("@function.macro" . "vscYellow")
              ("@parameter" . "vscLightBlue")
              ("@parameter.reference" . "vscLightBlue")
              ("@function.method" . "vscYellow")
              ("@field" . "vscLightBlue")
              ("@property" . "vscLightBlue")
              ("@constructor" . "vscBlueGreen")
              ("@conditional" . "vscPink")
              ("@repeat" . "vscPink")
              ("@label" . "vscLightBlue")
              ("@keyword" . "vscPink")
              ("@keyword.function" . "vscBlue")
              ("@keyword.operator" . "vscBlue")
              ("@operator" . "vscFront")
              ("@exception" . "vscPink")
              ("@type" . "vscBlueGreen")
              ("@type.builtin" . "vscBlue")
              ("@structure" . "vscLightBlue")
              ("@include" . "vscPink")
              ("@variable" . "vscLightBlue")
              ("@variable.builtin" . "vscLightBlue")
              ("@text" . "vscFront")
              ("@text.underline" . "vscYellowOrange")
              ("@tag" . "vscBlue")
              ("@tag.delimiter" . "vscGray")
              ("@tag.attribute" . "vscLightBlue")
              ("@text.title" . "vscYellowOrange")
              ("@text.literal" . "vscFront")
              ("markdown@text.literal" . "vscOrange")
              ("markdown_inline@text.literal" . "vscOrange")
              ("@text.emphasis" . "vscFront")
              ("@text.strong" . "vscViolet")
              ("@text.uri" . "vscFront")
              ("@textReference" . "vscYellowOrange")
              ("@punctuation.delimiter" . "vscFront")
              ("@stringEscape" . "vscYellowOrange")
              ("@text.note" . "vscBlueGreen")
              ("@text.warning" . "vscYellowOrange")
              ("@text.danger" . "vscRed")))
 ("colors" . (("vscNone" . "#000000")
              ("vscFront" . "#343434")
              ("vscBack" . "#FFFFFF")
              ("vscTabCurrent" . "#FFFFFF")
              ("vscTabOther" . "#CECECE")
              ("vscTabOutside" . "#E8E8E8")
              ("vscLeftDark" . "#F3F3F3")
              ("vscLeftMid" . "#E5E5E5")
              ("vscLeftLight" . "#F3F3F3")
              ("vscPopupFront" . "#000000")
              ("vscPopupBack" . "#F3F3F3")
              ("vscPopupHighlightBlue" . "#0064c1")
              ("vscPopupHighlightGray" . "#767676")
              ("vscSplitLight" . "#EEEEEE")
              ("vscSplitDark" . "#DDDDDD")
              ("vscSplitThumb" . "#DFDFDF")
              ("vscCursorDarkDark" . "#E5EBF1")
              ("vscCursorDark" . "#6F6F6F")
              ("vscCursorLight" . "#767676")
              ("vscSelection" . "#ADD6FF")
              ("vscLineNumber" . "#098658")
              ("vscDiffRedDark" . "#FFCCCC")
              ("vscDiffRedLight" . "#FFA3A3")
              ("vscDiffRedLightLight" . "#FFCCCC")
              ("vscDiffGreenDark" . "#DBE6C2")
              ("vscDiffGreenLight" . "#EBF1DD")
              ("vscSearchCurrent" . "#A8AC94")
              ("vscSearch" . "#F8C9AB")
              ("vscGitAdded" . "#587c0c")
              ("vscGitModified" . "#895503")
              ("vscGitDeleted" . "#ad0707")
              ("vscGitRenamed" . "#007100")
              ("vscGitUntracked" . "#007100")
              ("vscGitIgnored" . "#8e8e90")
              ("vscGitStageModified" . "#895503")
              ("vscGitStageDeleted" . "#ad0707")
              ("vscGitConflicting" . "#ad0707")
              ("vscGitSubmodule" . "#1258a7")
              ("vscContext" . "#D2D2D2")
              ("vscContextCurrent" . "#929292")
              ("vscFoldBackground" . "#e6f3ff")
              ("vscGray" . "#000000")
              ("vscViolet" . "#000080")
              ("vscBlue" . "#0000FF")
              ("vscDarkBlue" . "#007ACC")
              ("vscLightBlue" . "#0451A5")
              ("vscGreen" . "#008000")
              ("vscBlueGreen" . "#16825D")
              ("vscLightGreen" . "#098658")
              ("vscRed" . "#FF0000")
              ("vscOrange" . "#C72E0F")
              ("vscLightRed" . "#A31515")
              ("vscYellowOrange" . "#800000")
              ("vscYellow" . "#795E26")
              ("vscPink" . "#AF00DB"))))

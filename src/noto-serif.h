#pragma once

#include <stddef.h>

extern unsigned char itl_font_data_noto_serif[];
extern size_t itl_font_data_noto_serif_len;

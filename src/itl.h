#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

struct ITLFontImageRendererCtx;

typedef struct ITLFontImageRendererCtx ITLFontImageRendererCtx;


enum ITLError {
  ITL_OK = 0,
  ITL_ERROR_FREETYPE = 1,
  ITL_ERROR_COLOR_INTERVALS = 2
};

typedef enum ITLError ITLError;

#ifdef __cplusplus
extern "C" {
#endif

  ITLFontImageRendererCtx * itl_font_image_renderer_ctx_new();
  void itl_font_image_renderer_ctx_free(ITLFontImageRendererCtx *ctx);
  void itl_font_image_renderer_ctx_set_margin(ITLFontImageRendererCtx* ctx, uint32_t margin);
  void itl_font_image_renderer_ctx_set_font_size(ITLFontImageRendererCtx* ctx, uint32_t font_size);
  ITLError itl_font_image_renderer_ctx_set_font(ITLFontImageRendererCtx* ctx, void* font_buffer, size_t font_buffer_len);
  ITLError itl_font_image_renderer_ctx_set_font_file(ITLFontImageRendererCtx* ctx, const char* path);
  void itl_font_image_renderer_ctx_set_line_spacing(ITLFontImageRendererCtx* ctx, float line_spacing);
  void itl_font_image_renderer_ctx_set_bounding_box(ITLFontImageRendererCtx* ctx, _Bool bounding_box);

  
  void itl_render_font_image_to_file(struct ITLFontImageRendererCtx *ctx,
                                     const char* font_filename,
                                     const char* out_filename,
                                     const char* text,
                                     size_t text_len,
                                     uint32_t margin,
                                     uint32_t font_size);

  typedef struct {
    uint32_t start;
    uint32_t stop;
    uint32_t rgba;
  } ITLColorInterval;

  ITLError itl_render_font_image(ITLFontImageRendererCtx *ctx,
                                 const char* text_input,
                                 size_t text_input_len,
                                 ITLColorInterval *color_intervals,
                                 size_t color_intervals_len,
                                 void** out_image_buffer,
                                 size_t* out_image_buffer_len,
                                 char** out_render_info,
                                 size_t* out_render_info_len);


#ifdef __cplusplus
}
#endif
